import { Canvas} from "@react-three/fiber";
import React from "react";
import classes from "./Viewer.module.css";
import * as THREE from "three";
import { OrbitControls } from "@react-three/drei";
import Para from "./Para";

const Cross = (props) => {
  return (
    <div
      className={classes.crossContainer}
      onClick={() => {
        props.onClick();
      }}
    >
      <div />
      <div />
    </div>
  );
};

const Viewer = ({  close, images,...props }) => {
  const [render, setRender] = React.useState(false);
  const [devicePixelRatio, setDevicePixelRatio] = React.useState();
  const [curr, setCurr] = React.useState(props.curr);
  React.useEffect(() => {
    if (!render) {
      setTimeout(() => {
        setRender(true);
      }, 200);
    }
  }, []);

  React.useEffect(() => {
    document.body.style.overflow = "hidden";
    const pixel = window.devicePixelRatio;
    setDevicePixelRatio(pixel);
  }, []);

  return (
    <>
      <div className={classes.backdrop}>
        <div
          className={`${classes.container} ${
            render && classes.translateContainer
          }`}
        >
          <Cross
            onClick={() => {
              setRender(false);
              document.body.style.overflow = "initial";
              setTimeout(() => {
                close();
              }, 500);
            }}
          />
          {/* <h1 className={classes.title}>360-view</h1> */}
          <div className={classes.canvas} id="canvas">
            <div className={classes.buttonContainer} id={"360-viewer"}>
              {images.map((item, i) => {
                return (
                  <div key={i} className={`${(curr===i)&&classes.clicked}`}  onClick={() => {setCurr(i);}} id={`button-360-${i + 1}`}>
                    {item.name}
                  </div>
                );
              })}
            </div>
            <Canvas
              camera={{ position: [10, 0, 0], fov: 75 }}
              dpr={devicePixelRatio}
              gl={{ antialias: false }}
              onCreated={({ gl }) => {
                gl.toneMapping = THREE.ACESFilmicToneMapping;
                gl.outputEncoding = THREE.sRGBEncoding;
              }}
            >
              <pointLight position={[0, 4, 0]} intensity={0.4} color="white" />
              <ambientLight intensity={0.7} />
              {images.map((item, i) => {
                return <React.Fragment key={i} >{(curr===i) && <Para image={item.src} key={i} />}</React.Fragment>;
              })}
              <OrbitControls maxDistance={250} minDistance={50} />
            </Canvas>
          </div>
        </div>
      </div>
    </>
  );
};

export default Viewer;
