import { Swiper, SwiperSlide } from "swiper/react";
import { useState } from "react";
import Carousel from 'react-bootstrap/Carousel';

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";

import { FreeMode, Navigation, Thumbs } from "swiper";
import { Button } from "@mui/material";
import { createTheme } from "@mui/system";
import Viewer from "../360Viewer/Viewer360";
const Slider = (props) => {
    const [thumbsSwiper, setThumbsSwiper] = useState(null);
    const [index, setIndex] = useState(0);

    const handleSelect = (selectedIndex, e) => {
      setIndex(selectedIndex);
    };
//   const data = [
//     {
//       img: "9.jpg",
//       avatar: "1.jpg",
//       title: "Liguid Wave",
//       author: "Sound Box",
//     },
//     {
//       img: "10.jpg",
//       avatar: "2.jpg",
//       title: "Liguid Wave",
//       author: "Sound Box",
//     },
//   ];


  return (
    <>
      <div className="galleries">
        <div className="detail-gallery">
          {/* <div className="product-image-slider">
            <Swiper
              spaceBetween={10}
              navigation={true}
              thumbs={{
                swiper:
                  thumbsSwiper && !thumbsSwiper.destroyed ? thumbsSwiper : null,
              }}
              modules={[FreeMode, Navigation, Thumbs]}
              slidesPerView="auto"
            >
              <SwiperSlide>
                <figure className="border-radius-20">
                  <img
                    className="slide-img"
                    src="/assets/imgs/page/homepage8/img-new.jpg"
                    alt="product image"
                  />
                  <Button 
                    className="button-360"
                    variant="contained"
                    onClick={()=>{
                      props.view360.set360(1);
                    }}
                  >
                  360
                  </Button>
                </figure>
              </SwiperSlide>
              <SwiperSlide>
                <figure className="border-radius-20">
                  <img
                    className="slide-img"
                    src="/assets/imgs/page/homepage8/img-new.jpg"
                    alt="product image"
                  />
                  <Button 
                    className="button-360"
                    variant="contained"
                    onClick={()=>{
                      props.view360.set360(2);
                    }}
                  >
                  360
                  </Button>
                </figure>
                
              </SwiperSlide>
              <SwiperSlide>
                <figure className="border-radius-20">
                  <img
                    className="slide-img"
                    src="/assets/imgs/page/homepage8/img-new.jpg"
                    alt="product image"
                  />
                  <Button 
                    className="button-360"
                    variant="contained"
                    onClick={()=>{
                      props.view360.set360(3);
                    }}
                  >
                  360
                  </Button>
                </figure>
              </SwiperSlide>
            </Swiper>
          </div> */}
          <div className="product-image-carousel">
            <Carousel activeIndex={index} onSelect={handleSelect} interval={null}>
              <Carousel.Item>
                <img
                  className=""
                  src="/assets/imgs/page/homepage8/img-new.jpg"
                  alt="First slide"
                />
                <Button 
                      className="button-360"
                      variant="contained"
                      onClick={()=>{
                        props.view360.set360(1);
                      }}
                    >
                    360
                </Button>
                {/* <Carousel.Caption>
                  <h3>First slide label</h3>
                  <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                </Carousel.Caption> */}
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className=""
                  src="/assets/imgs/page/homepage8/img-new.jpg"
                  alt="Second slide"
                />
                <Button 
                      className="button-360"
                      variant="contained"
                      onClick={()=>{
                        props.view360.set360(1);
                      }}
                    >
                    360
                </Button>
                {/* <Carousel.Caption>
                  <h3>Second slide label</h3>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </Carousel.Caption> */}
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className=""
                  src="/assets/imgs/page/homepage8/img-new.jpg"
                  alt="Third slide"
                />
                <Button 
                      className="button-360"
                      variant="contained"
                      onClick={()=>{
                        props.view360.set360(1);
                      }}
                    >
                    360
                </Button>
                {/* <Carousel.Caption>
                  <h3>Third slide label</h3>
                  <p>
                    Praesent commodo cursus magna, vel scelerisque nisl consectetur.
                  </p>
                </Carousel.Caption> */}
              </Carousel.Item>
            </Carousel>
          </div>
          
        </div>
        <div className="slider-nav-thumbnails">
          <Swiper
            onSwiper={setThumbsSwiper}
            spaceBetween={10}
            slidesPerView={4}
            freeMode={true}
            watchSlidesProgress={true}
            modules={[FreeMode, Navigation, Thumbs]}
            className="slider-nav-thumbnails-align"
          >
            <SwiperSlide onClick={()=>{setIndex(0)}}>
              {/* <div> */}
              <img
                src="/assets/imgs/page/homepage8/img-new.jpg"
                alt="product image"
              />
              {/* </div> */}
            </SwiperSlide>
            <SwiperSlide onClick={()=>{setIndex(1)}}>
              {/* <div> */}
              <img
                src="/assets/imgs/page/homepage8/img-new.jpg"
                alt="product image"
              />
              {/* </div> */}
            </SwiperSlide>
            <SwiperSlide onClick={()=>{setIndex(2)}}>
              {/* <div> */}
              <img
                src="/assets/imgs/page/homepage8/img-new.jpg"
                alt="product image"
              />
              {/* </div> */}
            </SwiperSlide>
          </Swiper>
        </div>
      </div>
    </>
  );
};

export default Slider;